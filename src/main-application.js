import { LitElement, html } from 'lit-element';

import {style} from './main-styles.js';

export class MainApplication extends LitElement {

  constructor() {
    super()
    this.addEventListener('add-buddy', this.onMessage)
  }

  onMessage(message) {
    if(message.type==='add-buddy') {
      this.shadowRoot.querySelector('buddies-list').addBuddy(message.detail.value)
    }    
  }

  render() {
    return html`
      ${style}
      <section class="container">
        <div>
          <my-title></my-title>
          <my-sub-title my-text="Wij vinden je product ❤️"></my-sub-title>
          <my-form></my-form>
        </div>
      <section>
    `
  }
}

customElements.define('main-application', MainApplication);
