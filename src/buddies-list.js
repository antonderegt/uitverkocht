import { LitElement, html } from 'lit-element'

import {style} from './main-styles.js';

export class BuddiesList extends LitElement {

  static get properties() {
    return {
      buddies: { type: Array }
    }
  }

  constructor() {
    super()
    this.buddies = [
      "🦊 Test",
      "🦁 Leo",
      "🐯 Tigrou"
    ]

    if(window.components == null) window.components = []
    window.components.push(this)

    this.addEventListener("new buddy", e => {
      //this.buddies.push(e.detail) will not work
      this.buddies = [...this.buddies, e.detail]
    })

  }

  addBuddy(buddy) {
    //console.log("I'm the list")
    this.buddies = [...this.buddies, buddy]
  }

  render(){
    return html`
      ${style}
      <div>
        ${this.buddies.map(buddy => 
          html`<h2 class="subtitle">${buddy}</h2>`
        )}
      </div>
    `
  }
}
customElements.define('buddies-list', BuddiesList)