import { LitElement, html } from 'lit-element';
import {style} from './main-styles.js';

export class MyForm extends LitElement {
  constructor() {
    super()
    this.email = "";
    this.product = "";
  }

render() {
    return html`
    ${style}
        <input id="email" type="email" placeholder="email" class="input"/>
        <br>
        <input id="product" type="text" placeholder="product" class="input"/>
        <div>
        <button @click="${this.clickHandler}" class="button">👋 Vind het voor me!</button>
      </div>
       `
    }

    clickHandler() {
        this.email = this.shadowRoot.getElementById("email").value;
        this.product = this.shadowRoot.getElementById("product").value;
        console.log(this.email);
        console.log(this.product);
    }

}
customElements.define('my-form', MyForm)